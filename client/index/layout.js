Template.layout.rendered = function() {
  $(document).ready(function(){
  	$('.right.menu.open').on("click",function(e){
          e.preventDefault();
  		$('.ui.vertical.menu').toggle();
  	});

  	$('.ui.dropdown').dropdown();
  });
  $(".ui.dropdown").dropdown()
}

Template.layout.events({
  'click .logout' : function() {
    console.log("logging out");
    Meteor.logout();
  },
  'click .add-animal' : function() {
    Router.go("newAnimal");
  },
  'click .write-story' : function() {
    Router.go("newStory");
  }
})
