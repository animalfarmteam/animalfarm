Template.animals.events({
  'click .animal' : function(e , t) {
    Router.go("animal" , {id:this._id});
  }
});


Template.animals.rendered = function() {
  $(document).ready(function (){
    // jQuery
    var $container = $('#animals');
    // initialize
    $container.masonry({
      columnWidth: 200,
      itemSelector: '.item'
    });
  })
}

Template.animals.helpers({
  types : function() {
    return _.uniq(_.pluck(Animals.find().fetch() , 'type'))
  }
})
