Template.animal.helpers({
  'lifespanHTML' : function() {
    console.log(this.lifespan);
    if(this.lifespan) {
      var lifespanInt = parseInt(this.lifespan);
      x = "<div class='ui image'><img src = '/cow11.png'/> </div>"
      y = "";
      for(i = 1 ; i <= lifespanInt ; i++ ) {
        y = y + x;
      }
      return y;
    }
  },

  'benefitsHTML' : function() {
    if(this == "dairy") {
      return "<div class='ui mini image'><img src = '/milk.png'/> </div>";
    }
    if(this == "clothing") {
      return "<div class='ui mini image'><img src = '/jacket.png'/> </div>";
    }
    if(this == "money") {
      return "<div class='ui mini image'><img src = '/money132.png'/> </div>";
    }
    if(this == "meat") {
      return "<div class='ui mini image'><img src = '/steak.png'/> </div>";
    }
    if(this == "breeding") {
      return "<div class='ui mini image'><img src = '/breeding.png'/> </div>";
    }
    if(this == "eggs") {
      return "<div class='ui mini image'><img src = '/eggs.png'/> </div>";
    }
  }

});
