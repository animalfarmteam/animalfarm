Template.newAnimal.helpers({
    'animals': function () {
      return Animals.find({});
    }
  });

  Template.newAnimal.events({
    'click #animalSubmit': function(){
      var animalName = $("#newName").val();
      var animalHeight = $("#newHeight").val();
      var animalWeight = $("#newWeight").val();
      var animalLife = $("#newLifespan").val();
      var animalGest = $("#newGestation").val();
      var gestMonth = $("#newGestationMonths").checked;
      if($("#newGestationMonths").checked) {var gestUnits="months"}
      else {var gestUnits="days"}
      // var gestUnits = [];
      // $('input[name=gestUnits]:checked').each(function() {
      // gestUnits.push($(this).val());
      // });
      var animalCountries = $("#newCountries").val();
      var animalTitle = $("#newTitle").val();
      var animalStory = $("#newStory").val();
      var animalFunFact = $("#newFunFact").val();
      var animalOpp = [];
      $('input[name=opp]:checked').each(function() {
      animalOpp.push($(this).val());
      });
      var animalNutrition = $('#newNutrition').val();
      var animalCare = $('#animalCare').val();
      var animalReferences = $('#newReferences').val();
      var res = Animals.insert({
        "species":animalName,
        "height":animalHeight,
        "weight":animalWeight,
        "lifespan":animalLife,
        "gestation":animalGest,
        "gestationUnits":gestUnits,
        "countries":animalCountries,
        "pageTitle":animalTitle,
        "story":animalStory,
        "funFact":animalFunFact,
        "financialOpp":animalOpp,
        "nutrition":animalNutrition,
        "care":animalCare,
        "references":animalReferences,
        "cfsImage" : Session.get("uploadedImage")
      });

      if(res) {
        Router.go("animal" , {id : res});
      } else {
        Router.go("animals");
      }

    },
    'change #fileToUpload': FS.EventHandlers.insertFiles(Images, {
      metadata: function (fileObj) {
        return {
          owner: Meteor.userId(),
          foo: "bar"
        };
      },
      after: function (error, fileObj) {
        console.log("Inserted", fileObj);
        Session.set("uploadedImage" , fileObj._id);
      }
    }),
  });

  Template.newAnimal.rendered = function() {
    Session.set("uploadedImage" , "");
    $('.ui.checkbox').checkbox();
    $('.ui.radio.checkbox').checkbox();
  }
