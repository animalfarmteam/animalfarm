Template.stories.events({
  'click .like.ui' : function() {
    console.log(this);
    //upvote!
    Stories.update({_id: this._id} , {$inc: {votes : 1}})
  },
  'click .animalselect' : function(e , t) {
    var selected = $(e.target).data("value");
    if(selected === "All") {
    $(".All").show();
    } else {
      $(".All").hide();
    }
    $("." + selected).show();
    $('#stories').masonry('layout');
  },
  'click .story' : function(e , t) {
    Router.go("story" , {id:this._id});
  }
})


Template.stories.rendered = function() {
  $(document).ready(function (){
    // jQuery
    var $container = $('#stories');
    // initialize
    $container.masonry({
      columnWidth: 200,
      itemSelector: '.item'
    });

    $('.ui.dropdown').dropdown();

  })
}

Template.stories.helpers({
  types : function() {
    return _.uniq(_.pluck(Stories.find().fetch() , 'storytype'))
  }
})
