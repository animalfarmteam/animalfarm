Template.newStory.rendered = function() {
  $('.ui.checkbox').checkbox();
  $('.ui.radio.checkbox').checkbox();
  Session.set("uploadedImage" , "");
  storyObj = {};
}

var storyObj = {};

Template.newStory.events({
  'click #submitStory' : function() {
    storyObj.title = $("#title").val();
    storyObj.story = $("#story").val();
    storyObj.storyby = $("#storyby").val();
    storyObj.storytype = $('input:radio[name=storytype]:checked').val();
    storyObj.cfsImage = Session.get("uploadedImage");
    Stories.insert(storyObj);

    Router.go("stories");
  },
  'change #fileToUpload': FS.EventHandlers.insertFiles(Images, {
    metadata: function (fileObj) {
      return {
        owner: Meteor.userId(),
        foo: "bar"
      };
    },
    after: function (error, fileObj) {
      console.log("Inserted", fileObj);
      Session.set("uploadedImage" , fileObj._id);
    }
  })
});
