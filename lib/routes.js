Router.map(function() {
  this.route('home', {
    path: '/'
  });

  this.route('about', {
    path: '/about'
  });

  this.route("animals",{
    path: '/animals',
    data: {
      animals :   Animals.find()
    }
  });

  this.route("animal",{
    path: '/animal/:id',
    data: function(){
      return Animals.findOne({_id: this.params.id})
    }
  });

  this.route("editAnimal",{
    path: '/editAnimal/:id',
    data: {
      //animal :  Animals.findOne({_id: this.params.id})
    }
  });

  this.route("newAnimal",{
    path: '/newAnimal'
  });


  this.route("stories",{
    path: '/stories',
    data: {
      stories : Stories.find()
    }
  });
  this.route("story",{
    path: '/story/:id',
    data: function(){
      return Stories.findOne({_id: this.params.id})
    }
  });

  this.route("newStory",{
    path: '/newStory'
  });
  this.route("admin",{
    path: '/admin'
  })
})
