Animals = new Mongo.Collection("animals");
Stories = new Mongo.Collection("stories");

FS.HTTP.setBaseUrl('/cfsimages');

Images = new FS.Collection("images", {
  stores: [new FS.Store.FileSystem("images")]
});
